package com.ddw.sp03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sp03Application {

	public static void main(String[] args) {
		SpringApplication.run(Sp03Application.class, args);
	}

}
