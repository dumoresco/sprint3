package com.ddw.sprint3.service;

import com.ddw.sprint3.models.Address;
import com.ddw.sprint3.repositories.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ClientService {

    private ClientRepository clientRepository;

    public Address createAddress(Address address){
        return clientRepository.save(address);
    }

    public List<Address> showAddress(){
        return clientRepository.findAll();
    }
}
