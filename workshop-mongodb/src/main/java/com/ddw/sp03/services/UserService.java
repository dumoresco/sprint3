package com.ddw.sp03.services;

import com.ddw.sp03.exceptions.userNotFound.UserNotFoundException;
import com.ddw.sp03.models.entities.User;
import com.ddw.sp03.models.mapper.user.request.UserRequest;
import com.ddw.sp03.models.mapper.user.request.UserRequestMapper;
import com.ddw.sp03.models.mapper.user.response.UserResponse;
import com.ddw.sp03.models.mapper.user.response.UserResponseMapper;
import com.ddw.sp03.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserResponse> allUsers() {
        List<User> list = userRepository.findAll();
        return list.stream()
                .map(UserResponseMapper::toResponse)
                .collect(Collectors.toList());
    }

    public Page<UserResponse> allUsersPageable(Pageable pageable) {
        Page<User> list = userRepository.findAll(pageable);
        return list.map(UserResponseMapper::toResponse);
    }

    public UserResponse findUserById(String id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Usuário não encontrado. Nada para mostrar."));
        return UserResponseMapper.toResponse(user);
    }

    public UserResponse createNewUser(UserRequest userRequest) {
        return Optional.ofNullable(userRequest)
                .map(UserRequestMapper::toEntity)
                .map(userRepository::save)
                .map(UserResponseMapper::toResponse)
                .orElse(null);
//        return UserResponseMapper.toResponse(userRepository.insert(UserRequestMapper.toEntity(userRequest)));
    }

//    public void deleteUser(String id) {
//        userRepository.findById(id)
//                .orElseThrow(() -> new UserNotFoundException("Usuário não encontrado. Nada foi deletado."));
//        userRepository.deleteById(id);
//    }

    public void deleteUsers(List<String> ids) {
        if (ids.isEmpty()) {
            userRepository.deleteAll();
        } else {
            for (String id : ids) {
                findUserById(id);
            }
            userRepository.deleteAllById(ids);
        }
    }

    public UserResponse updateUser(UserRequest userRequest) {
        return userRepository.findById(userRequest.getId())
                .map((User user) -> UserRequestMapper.toEntity(userRequest))
                .map(userRepository::save)
                .map(UserResponseMapper::toResponse)
                        .orElseThrow(() -> new UserNotFoundException("Usuário não encontrado. Nada foi alterado."));
//        userToUpdate.setName(userRequest.getName());
//        userToUpdate.setEmail(userRequest.getEmail());
//        userRepository.save(UserRequestMapper.toEntity(userRequest));
//        return Optional.ofNullable(UserResponseMapper.toResponse();
    }
}

