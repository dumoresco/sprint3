package com.ddw.sp03.controller;

import com.ddw.sp03.models.mapper.user.request.UserRequest;
import com.ddw.sp03.models.mapper.user.response.UserResponse;
import com.ddw.sp03.services.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("v1/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
//    @ResponseStatus(HttpStatus.OK)
    public List<UserResponse> showAllUsers() {
        return userService.allUsers();
    }

    @GetMapping("/all/sort")
//    @ResponseStatus(HttpStatus.OK)
    public Page<UserResponse> showAllUsersPageable(Pageable pageable) {
        return userService.allUsersPageable(pageable);
    }


    @GetMapping("/{id}")
//    @ResponseStatus(HttpStatus.OK)
    public UserResponse findById(@PathVariable String id) {
        return userService.findUserById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponse createNewUser(@Valid @RequestBody UserRequest userRequest) {
        return userService.createNewUser(userRequest);
    }

//    @DeleteMapping("/{id}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void deleteUser(@PathVariable String id) {
//        userService.deleteUser(id);
//    }

    @DeleteMapping()
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUsers(@RequestParam List<String> ids) {
        userService.deleteUsers(ids);
    }

    @PutMapping()
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateUser(@RequestBody UserRequest userRequest) {
        userService.updateUser(userRequest);
    }

    @GetMapping()
    public String insertCookie(HttpServletResponse response) {
        Cookie cookie = new Cookie("userCookie", "1001");
        cookie.setMaxAge(60 * 60);
        response.addCookie(cookie);
        return "Cookie adicionado.";
    }
}
