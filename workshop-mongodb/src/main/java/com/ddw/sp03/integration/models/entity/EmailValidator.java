package com.ddw.sp03.integration.models.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Document(collection = "validatedEmails")
public class EmailValidator {

        @Id
        public String id;
        public String email;
//        public String autocorrect;
        public String deliverability;

}
