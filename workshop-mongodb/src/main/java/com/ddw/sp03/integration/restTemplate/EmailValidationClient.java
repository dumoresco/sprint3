package com.ddw.sp03.integration.restTemplate;

import com.ddw.sp03.integration.models.mapper.request.EmailValidationRequest;
import com.ddw.sp03.integration.models.mapper.response.EmailValidationResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class EmailValidationClient {

    private final RestTemplate restTemplate;

    public EmailValidationClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public EmailValidationResponse validation(EmailValidationRequest emailValidationRequest) {
        return this.restTemplate.getForObject("/v1/?api_key=467c154a296747a6b9609a7683072383&email=" +
                emailValidationRequest.getEmail(), EmailValidationResponse.class);
    }
}
