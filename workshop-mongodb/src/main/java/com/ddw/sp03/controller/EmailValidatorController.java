package com.ddw.sp03.controller;


import com.ddw.sp03.integration.models.entity.EmailValidator;
import com.ddw.sp03.integration.models.mapper.request.EmailValidationRequest;
import com.ddw.sp03.integration.models.mapper.response.EmailValidationResponse;
import com.ddw.sp03.services.EmailValidationService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/email-validator")
public class EmailValidatorController {

    private final EmailValidationService emailValidatorService;

    public EmailValidatorController(EmailValidationService emailValidatorService) {
        this.emailValidatorService = emailValidatorService;
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public EmailValidationResponse emailIsValid(@RequestParam("email") EmailValidationRequest email) {
        return emailValidatorService.validateEmail(email);
    }

    @GetMapping("/all-emails")
    @ResponseStatus(HttpStatus.OK)
    public List<EmailValidationResponse> allEmails(){
        return emailValidatorService.allValidatedEmails();
    }
}
