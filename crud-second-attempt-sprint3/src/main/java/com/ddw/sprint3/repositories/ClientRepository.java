package com.ddw.sprint3.repositories;

import com.ddw.sprint3.models.Address;
import com.ddw.sprint3.models.Client;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends MongoRepository<Address, String> {

}
