package com.ddw.sprint3;

import com.ddw.sprint3.models.Address;
import com.ddw.sprint3.models.Client;
import com.ddw.sprint3.models.Gender;
import com.ddw.sprint3.repositories.ClientRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
public class CrudSecondAttemptSprint3Application {

	public static void main(String[] args) {
		SpringApplication.run(CrudSecondAttemptSprint3Application.class, args);
	}
}



