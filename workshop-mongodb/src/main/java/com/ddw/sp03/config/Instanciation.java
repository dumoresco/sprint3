package com.ddw.sp03.config;

import com.ddw.sp03.models.entities.User;
import com.ddw.sp03.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@Configuration
public class Instanciation implements CommandLineRunner {

   private final UserRepository userRepository;

    public Instanciation(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        userRepository.deleteAll();

        User maria = new User(null, "Maria M Monson", "maria@gmail.com", LocalDateTime.of(2015, 12, 22, 15, 33));
        User alex = new User(null, "Alex Al Green", "alex@gmail.com", LocalDateTime.of(2021,10,20, 9, 20));
        User bob = new User(null, "Bob B Bronson", "bob@gmail.com", LocalDateTime.now());
        User jimi = new User(null, "Jimi J Jow", "jjj@gmail.com", null);

        userRepository.saveAll(Arrays.asList(maria, alex, bob, jimi));

    }
}
