package com.ddw.sp03.models.mapper.user.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserResponse {

    private String id;
    private String name;
    private String email;
    @JsonFormat (pattern = "dd-MM-yyyy, HH:mm")
    private LocalDateTime registrationDate;

}
