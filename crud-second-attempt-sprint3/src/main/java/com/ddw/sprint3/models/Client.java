package com.ddw.sprint3.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


@Data
@Document
public class Client {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private Gender gender;
    private Address address;
    private List<String> favouriteSearches;
    private BigDecimal totalSpent;
    private LocalDateTime created;

    public Client(String firstName, String lastName, String email, Gender gender, Address address, List<String> favouriteSearches, BigDecimal totalSpent, LocalDateTime created) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.gender = gender;
        this.address = address;
        this.favouriteSearches = favouriteSearches;
        this.totalSpent = totalSpent;
        this.created = created;
    }
}
