package com.ddw.sp03.integration.models.mapper.request;

import com.ddw.sp03.integration.models.entity.EmailValidator;
import lombok.experimental.UtilityClass;

import java.util.Optional;

@UtilityClass
public class EmailValidationRequestMapper {

    public static EmailValidator toEmailValidationRequest(EmailValidationRequest emailValidationRequest) {
        return Optional.ofNullable(emailValidationRequest)
                .map(emailValidatorService -> EmailValidator.builder()
                        .email(emailValidatorService.getEmail())
                        .build())
                .orElse(null);
    }
}
