package com.ddw.sp03.integration.models.mapper.response;

import com.ddw.sp03.integration.models.entity.EmailValidator;
import lombok.experimental.UtilityClass;

@UtilityClass
public class EmailValidationResponseMapper {

    public static EmailValidator mapToEmailValidator(EmailValidationResponse emailValidationResponse) {
        return EmailValidator.builder()
                .email(emailValidationResponse.getEmail())
                .deliverability(emailValidationResponse.getDeliverability())
                .build();
    }

    public static EmailValidationResponse toResponse(EmailValidator emailValidator) {
        return EmailValidationResponse.builder()
                .id(emailValidator.getId())
                .email(emailValidator.getEmail())
                .deliverability(emailValidator.getDeliverability())
                .build();
    }
}
