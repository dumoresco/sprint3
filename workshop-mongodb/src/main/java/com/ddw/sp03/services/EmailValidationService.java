package com.ddw.sp03.services;

import com.ddw.sp03.integration.models.entity.EmailValidator;
import com.ddw.sp03.integration.models.mapper.request.EmailValidationRequest;
import com.ddw.sp03.integration.models.mapper.response.EmailValidationResponse;
import com.ddw.sp03.integration.models.mapper.response.EmailValidationResponseMapper;
import com.ddw.sp03.integration.restTemplate.EmailValidationClient;
import com.ddw.sp03.models.entities.User;
import com.ddw.sp03.models.mapper.user.response.UserResponse;
import com.ddw.sp03.models.mapper.user.response.UserResponseMapper;
import com.ddw.sp03.repository.EmailValidationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.ddw.sp03.integration.models.mapper.response.EmailValidationResponseMapper.mapToEmailValidator;
import static com.ddw.sp03.integration.models.mapper.response.EmailValidationResponseMapper.toResponse;

@AllArgsConstructor
@Service
public class EmailValidationService {

    private final EmailValidationRepository emailValidationRepository;

    private final EmailValidationClient emailValidationClient;

    public EmailValidationResponse validateEmail(EmailValidationRequest emailValidationRequest) {
        return toResponse(emailValidationRepository.save(mapToEmailValidator(emailValidationClient.validation(emailValidationRequest))));
    }

    public List<EmailValidationResponse> allValidatedEmails() {
        List<EmailValidator> list = emailValidationRepository.findAll();
        List<EmailValidationResponse> listResponse =
                list.stream()
                        .map(x -> EmailValidationResponseMapper.toResponse(x))
                        .collect(Collectors.toList());
        return listResponse;
    }
}
