package com.ddw.sp03.models.mapper.user.response;

import com.ddw.sp03.models.entities.User;

public class UserResponseMapper {

    public static UserResponse toResponse(User user) {
        return UserResponse.builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .registrationDate(user.getRegistrationDate())
                .build();
    }
}
