package com.ddw.sp03.repository;

import com.ddw.sp03.integration.models.entity.EmailValidator;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailValidationRepository extends MongoRepository<EmailValidator, String> {
}
