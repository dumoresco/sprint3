package com.ddw.sp03.models.mapper.user.request;

import com.ddw.sp03.models.entities.User;
import lombok.experimental.UtilityClass;

import java.time.LocalDate;
import java.time.LocalDateTime;

@UtilityClass
public class UserRequestMapper {

    public static User toEntity(UserRequest userRequest) {
        return User.builder()
                .name(userRequest.getName())
                .email(userRequest.getEmail())
                .registrationDate(LocalDateTime.now())
                .build();
    }
}
