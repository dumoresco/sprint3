package com.ddw.sp03.exceptions.userNotFound;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String msg) {
        super(msg);
    }
}

