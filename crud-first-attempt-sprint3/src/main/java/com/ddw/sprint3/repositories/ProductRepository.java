package com.ddw.sprint3.repositories;

import com.ddw.sprint3.models.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, String> {

    @Override
    public void delete(Product product);

}
