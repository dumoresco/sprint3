package com.ddw.sp03.integration.restTemplate;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfiguration {

        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplateBuilder()
                    .rootUri("https://emailvalidation.abstractapi.com")
                    .build();

        }
}
