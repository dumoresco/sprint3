package com.ddw.sp03.integration.models.mapper.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class EmailValidationRequest {

    @JsonProperty
    private String email;

}
