package entities;

import lombok.*;

import java.util.Objects;

@NoArgsConstructor @AllArgsConstructor
@ToString @EqualsAndHashCode
public class Games implements Comparable<Games> {

    @Getter public static int numeroDeGames;
    @Getter @Setter private String nome;
    @Getter @Setter private int ano;
    @Getter @Setter private long id;

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Games games = (Games) o;
//        return ano == games.ano && id == games.id && Objects.equals(nome, games.nome);
//    }

//    @Override
//    public int hashCode() {
//        return Objects.hash(nome, ano, id);
//    }

//    @Override
//    public String toString() {
//        return "{ nome = '" + nome + '\'' +
//                ", ano = " + ano +
//                ", id = " + id +
//                " } ";
//    }

    @Override
    public int compareTo(Games o) {
        return ((Integer) this.ano).compareTo(o.getAno());
    }

}
