package com.ddw.sprint3.controller;

import com.ddw.sprint3.models.Address;
import com.ddw.sprint3.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping({"/address"})
public class ClientController {

    private ClientService clientService;


    @PostMapping("/create")
    public Address createAddress(@RequestBody Address address) {
        return this.clientService.createAddress(address);
    }

    @GetMapping("/all")
    public List<Address> showAllAddress(){
        return this.clientService.showAddress();
    }
}
