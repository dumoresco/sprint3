package com.ddw.sp03.integration.models.mapper.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EmailValidationResponse {

    private String id;

    @JsonProperty("email")
    private String email;

    @JsonProperty("deliverability")
    private String deliverability;

}
